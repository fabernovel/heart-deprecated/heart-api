## [1.0.1] - 2019-07-15
### Changed
- Heart wiki url from the README now properly redirect to the repository used before _Heart_ version 3

## [1.0.0] - 2019-06-11
### Added
- First release with the new module name (old one was _Heart Server_)
